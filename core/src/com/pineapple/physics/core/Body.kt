package com.pineapple.physics.core

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Pool
import ktx.collections.gdxArrayOf
import ktx.math.vec2

class Body() : Pool.Poolable {
    //No olvidar resetear los parametros en reset
    var posicionx : Float = 0f
    var posiciony : Float =0f
    var tamanioX : Float =1f
        set(value){
            if(value > 0){
                field = value
            } else{
                Gdx.app.error("Tamaño Entidad","El tamaño no puede ser menor o igual a 0")
                field =  1f
            }
        }

    var tamanioY : Float=1f
        set(value){
             if(value > 0){
                 field =   value
            } else{
                Gdx.app.error("Tamaño Entidad","El tamaño no puede ser menor o igual a 0")
                 field =  1f
            }
        }
    var velocidad = vec2(0f,0f)
    var aceleracion = vec2(0f,0f)
    var gravedad = true
    var solid = true
    var colisionElements = gdxArrayOf<Body>()
    var startColision : (body : Body) -> Unit = {}
    var endColision : (body : Body) -> Unit = {}
    var colisionTile : () -> Unit = {}
    var isGrounded = false
    var continuousCol = true
    var userData : Any? = null

    constructor(posicionX : Float, posicionY : Float, tamanioX : Float, tamanioY : Float) : this() {
        set(posicionX,posicionY,tamanioX,tamanioY)
    }
    fun set(posicionx : Float, posiciony : Float, tamanioX : Float, tamanioY : Float){
        this.posicionx = posicionx
        this.posiciony = posiciony
        this.tamanioX = tamanioX
        this.tamanioY = tamanioY
    }

    override fun reset() {
         posicionx  = 0f
         posiciony  =0f
         tamanioX  =1f
         tamanioY =1f
         velocidad.set(0f,0f)
         aceleracion.set(0f,0f)
         gravedad = true
         solid = true
         colisionElements.clear()
         startColision= {}
         endColision = {}
         colisionTile = {}
         isGrounded = false
         continuousCol = true
    }


}