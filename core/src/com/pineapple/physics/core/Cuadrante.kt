package com.pineapple.physics.core

import ktx.collections.gdxArrayOf

class Cuadrante(var x :Int,var y:Int) {

    val entidades = gdxArrayOf<Body>(ordered = false)
    override fun toString(): String {
        return "$x $y"
    }
}